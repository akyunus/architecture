# Diagram 1
The Project consists of three sub project.

```mermaid
graph TD
    C[Consumer Mobile App] ---> R[RestAPI / Web Service];
    A[Administration App] --> R;
    M[Manufacturer App] --> R;
    R --> DB[Database / MongoDB / FireStore];
    R --> AU[Auth Service / Firebase];
    C ---> AU;
    classDef App fill:#6e9,stroke:#333,stroke-width:4px;
    class A,R,M,C App;
```



## Diagram 2

Mobile Application MVVM pattern

```mermaid
classDiagram
    class View {
        Pages
        Components 
        Widgets
        Inject ViewModel
        DTO
    }
    class ViewModel {
        Facade Class
        Triggered_by View
        DTO Definitions
        State
        Configurations
        Interfaces--Services
        Error_Handling
    }
    class Model {
        +Data Structure
        +Serialization
        +Validation
        +Business Logic
        +Interfaces_for_Services
    }
    class Infrastructure{
        Mocks
        API Clients
        ThirdParty Services
        DB Clients
    }
    class ApplicationServices{
        Navigation
        Localization
        App Preferences
        Theme
    }
    Model <--> ViewModel
    View <--> ViewModel
    Model --> Infrastructure
    ViewModel <-- ApplicationServices
```
